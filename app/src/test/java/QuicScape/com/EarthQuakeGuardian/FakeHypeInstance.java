package quicscape.com.earthquakeguardian;

import com.hypelabs.hype.Instance;


public class FakeHypeInstance extends Instance {
    protected FakeHypeInstance(byte[] identifier, byte[] announcement, boolean isResolved) {
        super(identifier, announcement, isResolved);
    }
}
