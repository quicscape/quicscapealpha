package quicscape.com.earthquakeguardian;

import com.google.gson.Gson;

import quicscape.com.earthquakeguardian.ShakeDetection.ShakeRecord;

/**
 * Modified by Ahmed, Kenji
 * a class for the direct message sent between devices using hype.
 */

public class ChatModel {
    public String message;
    public String deviceName;
    public String time;
    public String batterylife;
    public String connectionlist;
    public Double longitude;
    public Double latitude;
    public String userImage;
    public String record;


    public ChatModel(String message, String deviceName, String time, String batterylife, String connectionlist, Double longitude, Double latitude, String userImage, ShakeRecord record) {
        this.message = message;
        this.deviceName = deviceName;
        this.time = time;
        this.batterylife = batterylife;
        this.connectionlist = connectionlist;
        this.longitude = longitude;
        this.latitude = latitude;
        this.userImage = userImage;

        Gson gson = new Gson();
        String stringshakerecord = gson.toJson(record);
        this.record = stringshakerecord;
        // will be changed to automatically get longitude and latitude from here.
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.message = time;
    }

    public String getBatterylife() {
        return batterylife;
    }

    public void setBatterylife(String batterylife) {
        this.batterylife = batterylife;
    }

    public String getConnectionlist() {
        return connectionlist;
    }

    public void setConnectionlist(String connectionlist) {
        this.connectionlist = connectionlist;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }


}
