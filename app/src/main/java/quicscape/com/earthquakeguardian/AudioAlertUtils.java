package quicscape.com.earthquakeguardian;
//Modified by Kenji
//Utils for playing audio in the app

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;

public class AudioAlertUtils {

    private static MediaPlayer mediaP = null;

    //play audio
    public void playBeep() {

        if (mediaP != null) {
            if (mediaP.isPlaying()) {
                return;
            }
        }
        mediaP = MediaPlayer.create(MainActivity.getContext(), R.raw.alert);
        AudioManager mAudioManager = (AudioManager) MainActivity.getContext().getSystemService(Context.AUDIO_SERVICE);
        mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);

        mediaP.setLooping(true);
        mediaP.start();

    }

    //stop audio
    public void stopBeep() {
        try {
            mediaP.reset();
            mediaP.prepare();
            mediaP.stop();
            mediaP.release();
            mediaP = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
