package quicscape.com.earthquakeguardian.WearDemo;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.hypelabs.hype.Message;

import java.lang.ref.WeakReference;

import quicscape.com.earthquakeguardian.ChattingSystem.ChatActivity;
import quicscape.com.earthquakeguardian.ChattingSystem.ChatApplication;
import quicscape.com.earthquakeguardian.ChattingSystem.Store;
import quicscape.com.earthquakeguardian.R;

public class WearResponderActivity extends AppCompatActivity implements Store.Delegate {

    private static final int REQUEST_ACCESS_COARSE_LOCATION_ID = 0;//Request code can be any number
    private String displayName;
    private static WeakReference<WearResponderActivity> defaultInstance;
    private ProgressBar progress_bar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        ListView listView;
        setTitle("Earthquake Guardian");
        final ChatApplication chatApplication = (ChatApplication) getApplication();
        final WearResponderActivity wearResponderActivity = this;
        chatApplication.setActivity(this);

        setContentView(R.layout.activity_wear_responder);

        listView = (ListView) findViewById(R.id.contact_view);

        progress_bar = (ProgressBar) findViewById(R.id.progressBar);
        progress_bar.setVisibility(View.VISIBLE);

        listView.setAdapter(new WearResponderViewAdapter(this, chatApplication.getStores(), new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                if (MotionEvent.ACTION_UP == motionEvent.getAction()) {

                    Intent intent = new Intent(WearResponderActivity.this, WearCompassActivity.class);

                    TextView displayName = (TextView) view.findViewById(R.id.hype_id);
                    CharSequence charSequence = displayName.getText();

                    setDisplayName(charSequence.toString());

                    Store store = chatApplication.getStores().get(getDisplayName());
                    store.setDelegate(wearResponderActivity);
                    intent.putExtra(ChatActivity.INTENT_EXTRA_STORE, store.getInstance().getStringIdentifier());

                    startActivity(intent);
                }

                return true;
            }
        }));

        // Gives access to ChatApplication for notifying when instances are found
        setWearResponderActivity(this);
    }

    @Override
    protected void onResume() {

        super.onResume();

        // Updates the UI on the press of a back button
        updateInterface();
    }

    @Override
    public void onMessageAdded(Store store, Message message) {

        updateInterface();
    }

    public String getDisplayName() {

        return displayName;
    }

    public void setDisplayName(String displayName) {

        this.displayName = displayName;
    }

    public static WearResponderActivity getDefaultInstance() {

        return defaultInstance != null ? defaultInstance.get() : null;
    }

    private static void setWearResponderActivity(WearResponderActivity instance) {

        defaultInstance = new WeakReference<>(instance);
    }

    public void notifyContactsChanged() {

        updateInterface();
    }

    public void notifyAddedMessage() {

        updateInterface();
    }

    protected void updateInterface() {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ListView listView = (ListView) findViewById(R.id.contact_view);
                updateHypeInstancesLabel(listView.getAdapter().getCount());

                ((WearResponderViewAdapter) listView.getAdapter()).notifyDataSetChanged();
            }
        });
    }

    private void updateHypeInstancesLabel(int nHypeInstances) {
        TextView hypeInstancesText = (TextView) findViewById(R.id.hps_text);

        if (nHypeInstances == 0)
            hypeInstancesText.setText("Searching for Nearby Devices ...");
        else
            hypeInstancesText.setText("Nearby Devices Found: " + nHypeInstances);
    }

    public void requestPermissions(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    REQUEST_ACCESS_COARSE_LOCATION_ID);
        } else {
            ChatApplication chatApplication = (ChatApplication) getApplication();
            chatApplication.requestHypeToStart();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ACCESS_COARSE_LOCATION_ID:

                ChatApplication chatApplication = (ChatApplication) getApplication();
                chatApplication.requestHypeToStart();
                break;
        }
    }
}
