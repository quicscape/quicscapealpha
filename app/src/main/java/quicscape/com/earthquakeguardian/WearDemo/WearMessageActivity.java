package quicscape.com.earthquakeguardian.WearDemo;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.BatteryManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.gson.Gson;
import com.hypelabs.hype.Hype;
import com.hypelabs.hype.Instance;
import com.hypelabs.hype.Message;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;

import quicscape.com.earthquakeguardian.AudioAlertUtils;
import quicscape.com.earthquakeguardian.ChatModel;
import quicscape.com.earthquakeguardian.ChattingSystem.ChatApplication;
import quicscape.com.earthquakeguardian.ChattingSystem.ChatViewAdapter;
import quicscape.com.earthquakeguardian.ChattingSystem.Store;
import quicscape.com.earthquakeguardian.R;

public class WearMessageActivity extends AppCompatActivity implements Store.Delegate {

    private TextView mTextView;
    private Button message1_btn, message2_btn, message3_btn;

    public static String INTENT_EXTRA_STORE = "com.hypelabs.store";

    // My Location
    private LocationManager locationManager;
    private Double myLat = 0.0;
    private Double myLon = 0.0;
    private Location myLocation;
    private static final int PERMISSION_CODE = 1; //Can be any random number
    private String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.CAMERA};// Permission to get picture and location

    private AudioAlertUtils soundAlert = new AudioAlertUtils();

    protected Message sendMessage(String text, Instance instance) throws UnsupportedEncodingException {

        // When sending content there must be some sort of protocol that both parties
        // understand. In this case, we simply send the text encoded in UTF-8. The data
        // must be decoded when received, using the same encoding.
        byte[] data = text.getBytes("UTF-8");

        // Sends the data and returns the message that has been generated for it. Messages have
        // identifiers that are useful for keeping track of the message's deliverability state.
        // In order to track message delivery set the last parameter to true. Notice that this
        // is not recommend, as it incurs extra overhead on the network. Use this feature only
        // if progress tracking is really necessary.
        return Hype.send(data, instance, false);
    }

    @Override
    protected void onStart() {
        super.onStart();

        setContentView(R.layout.activity_wear_message);
        setTitle("EG Wear Demo");
        mTextView = (TextView) findViewById(R.id.connected_device_name);
        message1_btn = findViewById(R.id.message1_btn);
        message2_btn = findViewById(R.id.message2_btn);
        message3_btn = findViewById(R.id.message3_btn);

        final Store store = getStore();

        String storeIdentifier = getIntent().getStringExtra(INTENT_EXTRA_STORE);

        mTextView.setText("Inform: " + storeIdentifier);
        message1_btn.setText("We are coming!");
        message2_btn.setText("Please make some noise!");
        message3_btn.setText("Please keep calm!");

        message1_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(WearMessageActivity.this, "Message Sent!", Toast.LENGTH_SHORT).show();
                sendMessage("We are coming!", store);
                finish();
            }
        });

        message2_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(WearMessageActivity.this, "Message Sent!", Toast.LENGTH_SHORT).show();
                sendMessage("Please make some noise!", store);
                finish();
            }
        });

        message3_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(WearMessageActivity.this, "Message Sent!", Toast.LENGTH_SHORT).show();
                sendMessage("Please keep calm!", store);
                finish();
            }
        });

        TextView announcement = findViewById(R.id.connected_device_name);
        try {
            announcement.setText("Send to " + new String(store.getInstance().getAnnouncement(), "UTF-8"));
            setTitle("Earthquake Guardian");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        // Start listening to message queue events so we can update the UI accordingly
        store.setDelegate(this);

        // Flag all messages as read
        store.setLastReadIndex(store.getMessages().size());
    }

    public void soundAlarm(Boolean toggle, Store store) {
        if (toggle == true) {
            sendMessage("#playsound", store);
        } else {
            sendMessage("#stopsound", store);
        }
    }

    public void sendMessage(String msg, Store store) {
        SimpleDateFormat formatter = new SimpleDateFormat("H:mm a");
        Date date = new Date();

        //BatteryManager bm = (BatteryManager)getSystemService(BATTERY_SERVICE);//gets current battery level.
        //int batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);// assigns it to a variable

        //will get the location from the setuplocation() method and send it to first responder (tbd)

        //String batterypercentage = Integer.toString(batLevel);

        //Setup message as a ChatModel and covert to JSON
        ChatModel model = new ChatModel(msg, "[FR]Wear OS " + ChatApplication.announcement, formatter.format(date), "N/A", "", myLon, myLat, null, null);
        Gson gson = new Gson();
        String text = gson.toJson(model);

        //Get GPS location for the device
        //setupLocation();

        // Avoids accidental presses
        if (text == null || text.length() == 0) {
            return;
        }
        //Try to send message
        try {
            Log.v(getClass().getSimpleName(), "send message");
            Message message = sendMessage(text, store.getInstance());

            if (message != null) {

                // Add the message to the store so it shows in the UI
                store.add(message);
            }

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    //Get GPS location for the device
    public void setupLocation() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        LocationListener locationListenerGPS = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                myLat = location.getLatitude();
                myLon = location.getLongitude();
                myLocation = location;
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };
        //Check for missing permissions
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 1, locationListenerGPS);
    }

    //Update listView when new message is added
    @Override
    public void onMessageAdded(Store store, Message message) {

//        this.runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                final ListView listView = (ListView) findViewById(R.id.list_view);
//
//                ((ChatViewAdapter) listView.getAdapter()).notifyDataSetChanged();
//            }
//        });
    }

    protected Store getStore() {// get the unique store required

        ChatApplication chatApplication = (ChatApplication) getApplication();
        String storeIdentifier = getIntent().getStringExtra(INTENT_EXTRA_STORE);
        Store store = chatApplication.getStores().get(storeIdentifier);

        return store;
    }
}
