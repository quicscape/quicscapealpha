package quicscape.com.earthquakeguardian.WearDemo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.hypelabs.hype.Message;

import java.io.UnsupportedEncodingException;
import java.util.Map;

import quicscape.com.earthquakeguardian.ChattingSystem.ChatApplication;
import quicscape.com.earthquakeguardian.ChattingSystem.Store;
import quicscape.com.earthquakeguardian.R;

public class WearResponderViewAdapter extends BaseAdapter {

    private Context context;
    private Map<String, Store> stores;
    private LayoutInflater inflater = null;
    private View.OnTouchListener onTouchListener;

    public WearResponderViewAdapter(Context context, Map<String, Store> stores, View.OnTouchListener onTouchListener) {

        this.context = context;
        this.stores = stores;
        this.onTouchListener = onTouchListener;
    }

    protected LayoutInflater getInflater() {

        if (inflater == null) {
            inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        return inflater;
    }

    protected Context getContext() {

        return context;
    }

    protected Map<String, Store> getStores() {

        return stores;
    }

    @Override
    public int getCount() {

        return stores.size();
    }

    @Override
    public Object getItem(int position) {

        // This way of getting ordered stores is an overkill, but that's not the point of this demo
        return getStores().values().toArray()[position];
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View vi = convertView;

        if (vi == null)
            vi = getInflater().inflate(R.layout.activity_wear_responder_view, null);

        Store store = (Store) getItem(position);

        boolean batterylifefound = false;
        TextView displayName = (TextView) vi.findViewById(R.id.hype_id);
        TextView announcement = (TextView) vi.findViewById(R.id.hype_announcement);
        TextView batterylevel = (TextView) vi.findViewById(R.id.battery_level);

        displayName.setText(store.getInstance().getStringIdentifier());
        //Filter through store data a display devices that sent their battery info
        if (store.getMessages().size() > 0) {
            for (int i = store.getMessages().size(); i-- > 0; ) {
                if (!batterylifefound) {
                    final Message message2 = store.getMessages().get(i);
                    try {
                        String dataMessage = new String(message2.getData(), "UTF-8");
                        if (!dataMessage.contains("N<C")) {
                            JsonObject jsonObject = new JsonParser().parse(dataMessage).getAsJsonObject();
                            if (!ChatApplication.announcement.equals(jsonObject.get("deviceName").getAsString())) {
                                batterylevel.setText("Battery Level: " + jsonObject.get("batterylife").getAsString() + "%");
                                batterylifefound = true;
                            }
                        }
                    } catch (Exception e) {

                    }
                }
            }
            final Message message2 = store.getMessages().get(store.getMessages().size() - 1);
            try {
                String dataMessage = new String(message2.getData(), "UTF-8");
                if (!dataMessage.contains("N<C")) {
                    JsonObject jsonObject = new JsonParser().parse(dataMessage).getAsJsonObject();
                    if (!ChatApplication.announcement.equals(jsonObject.get("deviceName").getAsString())) {
                        batterylevel.setText("Battery Level: " + jsonObject.get("batterylife").getAsString() + "%");
                    }

                }
            } catch (Exception e) {

            }
        }

        try {
            announcement.setText(new String(store.getInstance().getAnnouncement(), "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            announcement.setText("");
        }

        vi.setOnTouchListener(getOnTouchListener());

        return vi;
    }

    protected View.OnTouchListener getOnTouchListener() {

        return onTouchListener;
    }
}