package quicscape.com.earthquakeguardian;
// Modified By Joo, Ahmed and Kenji

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.format.Time;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import quicscape.com.earthquakeguardian.ChattingSystem.ContactActivity;
import quicscape.com.earthquakeguardian.ShakeDetection.DialogCreator;
import quicscape.com.earthquakeguardian.ShakeDetection.Installation;
import quicscape.com.earthquakeguardian.ShakeDetection.LocalDatabase;
import quicscape.com.earthquakeguardian.ShakeDetection.ResponderActivity;
import quicscape.com.earthquakeguardian.ShakeDetection.ShakeDetector;
import quicscape.com.earthquakeguardian.ShakeDetection.ShakeRecord;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import quicscape.com.earthquakeguardian.WearDemo.WearActivity;
import quicscape.com.earthquakeguardian.WearDemo.WearResponderActivity;

//Main page for Civilian and First Responder
public class MainActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks, DialogCreator.NoticeDialogListener, LocationListener {
    private HypePubSub hps = HypePubSub.getInstance();
    private Network network = Network.getInstance();
    private HypeSdkInterface hypeSdk = HypeSdkInterface.getInstance();
    private UIData uiData = UIData.getInstance();

    //Shake detection variables
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private ShakeDetector mShakeDetector;
    private Context mycontext = this;//Shake End
    public Location location;
    private ShakeRecord shakeRecord;
    private Installation installation;
    private JSONArray RecordsReceived;
    private LocationManager locationManager;
    boolean isGPSEnabled;
    boolean isNetworkEnabled;
    boolean locationServiceAvailable;
    private DialogFragment newFragment;
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 0; // 10 meters
    private static final long MIN_TIME_BW_UPDATES = 0;//1000 * 60 * 1; // 1 minute
    final static String TAG = "Mainactivity";

    //testing
    private JSONObject jsonBody;//Shake
    private RequestQueue queue;
    private final int REQUEST_LOCATION_PERMISSION = 1;

    private LinearLayout subscribeButton;
    //private Button unsubscribeButton;
    private LinearLayout publishButton;
    //private Button checkOwnIdButton;
    private LinearLayout checkHypeDevicesButton;
    //private Button checkOwnSubscriptionsButton;
    //private Button checkManagedServicesButton;
    private LinearLayout firstResponderButton;
    private LinearLayout wearResponderButton;
    private LinearLayout mainlinearLayout;
    private LinearLayout mainlinearLayoutsub;
    private LinearLayout mainlinearLayout2;
    private LinearLayout watchlayout;
    private TextView checkHypeDeviceTextview;
    private TextView mainActivityTextview;
    private TextView broadcastTextview;

    private ImageView mainactivityImageView;

    //Variable for who is the user.
    private String user;
    private boolean joinNet;
    private Location mylocation;
    private FusedLocationProviderClient fuseLocationClient;


    private static MainActivity instance; // Way of accessing the application context from other classes

    public MainActivity() {
        instance = this;
    }

    public static Context getContext() {
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //intialize sharedPreferences for user and join network button
        intializeSharedPreferences(false);

        //Setting up for buttons and click listeners.
        initButtonsFromResourceIDs();

        setButtonListeners();

        // Initialize hype SDK
        if (uiData.isToInitializeSdk) {
            initHypeSdk();
            uiData.isToInitializeSdk = false;
        }

        requestLocationPermission();

        initLocationService();

        fuseLocationClient = LocationServices.getFusedLocationProviderClient(this);

        fuseLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            // Logic to handle location object
                            mylocation = location;
                        }
                    }
                });


        //Shake Detection. Get the manager and save the user info.
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager
                .getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mShakeDetector = new ShakeDetector();

        mShakeDetector.setOnShakeListener(new ShakeDetector.OnShakeListener() {

            @Override
            public void onShake(double gForce, double gX, double gY, double gZ, String time) {

                if (location == null) {
                    initLocationService();
                }
                double shakelatitude = mylocation.getLatitude();
                double shakelongitude = mylocation.getLongitude();

                //detect and call api
                Time now = new Time();
                now.setToNow();
                Toast.makeText(mycontext, "shake detected: Gforce-->" + gForce + "\n Xaxis-->" + gX + "\n Yaxis-->" + gY + " \n Zaxis-->" + gZ + " \n Date-->" + time, Toast.LENGTH_LONG).show();
                shakeRecord = new ShakeRecord("", installation.id(mycontext), "", gForce, gX, gY, gZ, shakelongitude, shakelatitude, now.toString(), "");
                try {
                    newFragment.dismiss();
                } catch (Exception e) {

                }
                newFragment = new DialogCreator();
                getSupportFragmentManager().beginTransaction().add(newFragment, "InjuryCondition").commitAllowingStateLoss();

                //newFragment.show(getSupportFragmentManager(), "InjuryCondition");

                getrecords();

                autosubscribe(); // subscribe to emergency broadcast when a shake is detected


                //  Toast.makeText(mycontext,installation.id(mycontext),Toast.LENGTH_LONG).show();

            }
        });

        queue = Volley.newRequestQueue(this);

    }

    private void initHypeSdk() {
        requestHypeRequiredPermissions(this);
    }

    //intialize sharedPreferences for user and join network button
    private boolean intializeSharedPreferences(Boolean joinBtn) {
        SharedPreferences sharedPref = getSharedPreferences("Earthquake_Gaurdian_User", Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedPref.edit();
        user = sharedPref.getString("User", "[N/A]");
        joinNet = sharedPref.getBoolean("joinNet", false);
        if (joinBtn == true) {
            editor.putBoolean("joinNet", true);
            editor.commit();
            return true;
        }
        return false;
    }

    private void setButtonListeners() {
        setListenerSubscribeButton();
        //setListenerUnsubscribeButton();
        setListenerPublishButton();
        //setListenerCheckOwnIdButton();
        setListenerCheckHypeDevicesButton();
        //setListenerCheckOwnSubscriptionsButton();
        //setListenerCheckManagedServicesButton();
        setListenerFirstResponderButton();
        setListenerWearResponderButton();
    }


    private void initButtonsFromResourceIDs() {
        subscribeButton = findViewById(R.id.activity_main_subscribe_button);
        //unsubscribeButton = findViewById(R.id.activity_main_unsubscribe_button);
        publishButton = findViewById(R.id.activity_main_publish_button);
        //checkOwnIdButton = findViewById(R.id.activity_main_check_own_id_button);
        checkHypeDevicesButton = findViewById(R.id.activity_main_check_hype_devices_button);
        //checkOwnSubscriptionsButton = findViewById(R.id.activity_main_check_own_subscriptions_button);
        //checkManagedServicesButton = findViewById(R.id.activity_main_check_managed_services_button);
        firstResponderButton = findViewById(R.id.activity_main_first_responder_button);
        wearResponderButton = findViewById(R.id.activity_main_wear_responder_button);
        checkHypeDeviceTextview = findViewById(R.id.activity_main_check_hype_devices_textview);
        broadcastTextview = findViewById(R.id.activity_main_subscribe_textview);
        mainActivityTextview = findViewById(R.id.mainActivity_textview);
        mainlinearLayout = findViewById(R.id.mainLinearLayout);
        mainlinearLayout2 = findViewById(R.id.mainLinearLayout2);
        mainlinearLayoutsub = findViewById(R.id.mainLinearLayoutEnable);
        mainactivityImageView = findViewById(R.id.mainActivityImage);
        watchlayout = findViewById(R.id.watchbuttonlayout);


        if (user.equals("[C]")) {
            setTitle("Civilian");
            mainlinearLayout.setWeightSum(0);
            mainlinearLayout.setOrientation(mainlinearLayout.VERTICAL);
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 0);
            mainlinearLayoutsub.setLayoutParams(param);
            mainlinearLayout2.setLayoutParams(param);
            //Change any size to DP.
            float density = mycontext.getResources().getDisplayMetrics().density;
            int paddingTop = 15;
            int changeSizetoDP = (int) (paddingTop * density);
            mainlinearLayout2.setPadding(0, changeSizetoDP, 0, 0);
            mainactivityImageView.setImageDrawable(ContextCompat.getDrawable(mycontext, R.drawable.civilianimage));
            // mainactivityImageView.setImageDrawable(ContextCompat.getDrawable(mycontext, R.drawable.civilianmain2));
            broadcastTextview.setTextSize(getResources().getDimension(R.dimen.civilianMainButtonTextSize));
            checkHypeDeviceTextview.setTextSize(getResources().getDimension(R.dimen.civilianMainButtonTextSize));
            publishButton.setVisibility(View.GONE);
            firstResponderButton.setVisibility(View.GONE);
            wearResponderButton.setVisibility(View.GONE);
            checkHypeDeviceTextview.setText("Search Nearby Users");
            mainActivityTextview.setText("Welcome. Civilian!");
        } else if (user.equals("[FR]")) {
            setTitle("First Responder");
            publishButton.setVisibility(View.VISIBLE);
            wearResponderButton.setVisibility(View.GONE);
            wearResponderButton.setVisibility(View.VISIBLE);
            checkHypeDeviceTextview.setText("Search Nearby Civilian");
            mainActivityTextview.setText("Welcome. First Responder!");
            broadcastTextview.setText("There is an Earthquake!");
        } else if (user.equals("[FR]Wear OS ")) {
            setTitle("First Responder");
            subscribeButton.setVisibility(View.GONE);
            publishButton.setVisibility(View.GONE);
            checkHypeDevicesButton.setVisibility(View.GONE);
            firstResponderButton.setVisibility(View.GONE);
            wearResponderButton.setVisibility(View.VISIBLE);
            checkHypeDeviceTextview.setText("Check for Nearby civilian");
            mainActivityTextview.setText("Welcome. First Responder!");
            watchlayout.setVisibility(View.VISIBLE);
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1);
            watchlayout.setLayoutParams(param);
        }

        if (joinNet == true) {
            broadcastTextview.setText("Joined Emergency Network");
            subscribeButton.setBackgroundResource(R.drawable.background_grey);
        }
    }

    //////////////////////////////////////////////////////////////////////////////
    // Button Listener Methods
    //////////////////////////////////////////////////////////////////////////////

    private void setListenerSubscribeButton() {
        subscribeButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (!isHypeSdkReady()) {
                    showHypeNotReadyDialog();
                    return;
                }

                displayServicesNamesList("Subscribe",
                        "Select a service to subscribe",
                        uiData.getUnsubscribedServicesAdapter(MainActivity.this),
                        new subscribeServiceAction(),
                        true);

                if (joinNet == false) {
                    broadcastTextview.setText("Joined Emergency Network");
                    subscribeButton.setBackgroundResource(R.drawable.background_grey);
                    intializeSharedPreferences(true);
                }
            }
        });
    }


    public void autosubscribe() {

        try {
            if (!isHypeSdkReady()) {
                showHypeNotReadyDialog();
                return;
            }
            if (joinNet == false) {
                displayServicesNamesList("Subscribe",
                        "Select a service to subscribe",
                        uiData.getUnsubscribedServicesAdapter(MainActivity.this),
                        new subscribeServiceAction(),
                        true);


                broadcastTextview.setText("Joined Emergency Network");
                subscribeButton.setBackgroundResource(R.drawable.background_grey);
                intializeSharedPreferences(true);
            }
        } catch (Exception e) {
            Log.e("err", e.toString());
        }

    }

    private void setListenerFirstResponderButton() {
        final Intent intent = new Intent(this, ResponderActivity.class);

        firstResponderButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (!isHypeSdkReady()) {
                    showHypeNotReadyDialog();
                    return;
                }
                if (RecordsReceived != null) {
                    intent.putExtra("recordsarray", RecordsReceived.toString());
                } else {

                    intent.putExtra("recordsarray", "");
                    Toast.makeText(mycontext, "Your list is empty. Check your internet connection to retrieve the data. ", Toast.LENGTH_LONG).show();
                }
                startActivity(intent);
            }
        });
    }


    private void setListenerWearResponderButton() {
        final Intent intent = new Intent(this, WearActivity.class);

        wearResponderButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (!isHypeSdkReady()) {
                    showHypeNotReadyDialog();
                    return;
                }
                startActivity(intent);
            }
        });
    }

//    private void setListenerUnsubscribeButton() {
//        unsubscribeButton.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View arg0)
//            {
//                if( !isHypeSdkReady()) {
//                    showHypeNotReadyDialog();
//                    return;
//                }
//                if( isNoServiceSubscribed()) {
//                    showNoServicesSubscribedDialog();
//                    return;
//                }
//
//                displayServicesNamesList("Unsubscribe",
//                        "Select a service to unsubscribe",
//                        uiData.getSubscribedServicesAdapter(MainActivity.this),
//                        new unsubscribeServiceAction(),
//                        false);
//            }
//        });
//    }

    private void setListenerPublishButton() {
        publishButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (!isHypeSdkReady()) {
                    showHypeNotReadyDialog();
                    return;
                }

                displayServicesNamesList("Publish",
                        "Select a service in which to publish",
                        uiData.getAvailableServicesAdapter(MainActivity.this),
                        new publishServiceAction(),
                        true);
            }
        });
    }

//    private void setListenerCheckOwnIdButton() {
//        checkOwnIdButton.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View arg0)
//            {
//                if( !isHypeSdkReady()) {
//                    showHypeNotReadyDialog();
//                    return;
//                }
//
//                AlertDialogUtils.showInfoDialog(MainActivity.this,"Own Device",
//                        HpsGenericUtils.getInstanceAnnouncementStr(network.ownClient.instance) + "\n"
//                                + HpsGenericUtils.getIdStringFromClient(network.ownClient) + "\n"
//                                + HpsGenericUtils.getKeyStringFromClient(network.ownClient));
//            }
//        });
//    }

    private void setListenerCheckHypeDevicesButton() {
        final Intent intent = new Intent(this, ContactActivity.class);

        checkHypeDevicesButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (!isHypeSdkReady()) {
                    showHypeNotReadyDialog();
                    return;
                }

                startActivity(intent);
            }
        });
    }

//    private void setListenerCheckOwnSubscriptionsButton() {
//        final Intent intent = new Intent(this, SubscriptionsListActivity.class);
//
//        checkOwnSubscriptionsButton.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View arg0)
//            {
//                if( !isHypeSdkReady()) {
//                    showHypeNotReadyDialog();
//                    return;
//                }
//                if( isNoServiceSubscribed()) {
//                    showNoServicesSubscribedDialog();
//                    return;
//                }
//
//                startActivity(intent);
//            }
//        });
//    }

//    private void setListenerCheckManagedServicesButton() {
//        final Intent intent = new Intent(this, ServiceManagersListActivity.class);
//
//        checkManagedServicesButton.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View arg0)
//            {
//                if( !isHypeSdkReady()) {
//                    showHypeNotReadyDialog();
//                    return;
//                }
//                MainActivity.this.
//                startActivity(intent);
//            }
//        });
//    }

    //////////////////////////////////////////////////////////////////////////////
    // User Action Processing Methods
    //////////////////////////////////////////////////////////////////////////////

    private void displayServicesNamesList(String title,
                                          String message,
                                          ListAdapter serviceNamesAdapter,
                                          final IServiceAction serviceAction,
                                          Boolean isNewServiceSelectionAllowed) {
        final ListView listView = new ListView(MainActivity.this);
        listView.setAdapter(serviceNamesAdapter);

        LinearLayout layout = new LinearLayout(MainActivity.this);
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.addView(listView);

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(title);
        builder.setCancelable(true);
        builder.setView(layout);
        builder.setMessage(message);
        builder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

//        if(isNewServiceSelectionAllowed) {
//            builder.setNeutralButton("New Service",
//                new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        processNewServiceSelection(serviceAction);
//                        dialog.dismiss();
//                    }
//                });
//        }

        final Dialog dialog = builder.create();
        //dialog.show();

//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                String listItem = (String) listView.getItemAtPosition(position);
//                serviceAction.action(listItem);
//                dialog.dismiss();
//            }
//        });
        serviceAction.action("Emergency Broadcast");
    }

    private interface IServiceAction {
        void action(String userInput);
    }

    private class subscribeServiceAction implements IServiceAction {
        @Override
        public void action(String userInput) {
            String serviceName = processUserServiceNameInput(userInput);
            if (serviceName.length() == 0) {
                return;
            }

            if (hps.ownSubscriptions.containsSubscriptionWithServiceName(serviceName)) {
                AlertDialogUtils.showInfoDialog(MainActivity.this,
                        "INFO", "Already joined broadcast network");
                return;
            }

            boolean wasSubscribed = hps.issueSubscribeReq(serviceName);
            if (wasSubscribed) {
                uiData.addSubscribedService(MainActivity.this, serviceName);
                uiData.removeUnsubscribedService(MainActivity.this, serviceName);
            }
        }
    }

//    private class unsubscribeServiceAction implements IServiceAction {
//        @Override
//        public void action(String userInput) {
//            String serviceName = processUserServiceNameInput(userInput);
//            if (serviceName.length() == 0) {
//                return;
//            }
//
//            boolean wasUnsubscribed = hps.issueUnsubscribeReq(serviceName);
//            if (wasUnsubscribed) {
//                uiData.addUnsubscribedService(MainActivity.this, serviceName);
//                uiData.removeSubscribedService(MainActivity.this, serviceName);
//            }
//        }
//    }

    private class publishServiceAction implements IServiceAction {
        @Override
        public void action(String userInput) {
            final String serviceName = processUserServiceNameInput(userInput);
            if (serviceName.length() == 0) {
                return;
            }

            AlertDialogUtils.ISingleInputDialog publishMsgInput = new AlertDialogUtils.ISingleInputDialog() {

                @Override
                public void onOk(String msg) {
                    msg = msg.trim();
                    if (msg.length() == 0) {
                        AlertDialogUtils.showInfoDialog(MainActivity.this,
                                "WARNING",
                                "A message must be specified");
                        return;
                    }

                    hps.issuePublishReq(serviceName, msg);
                }

                @Override
                public void onCancel() {
                }
            };


            AlertDialogUtils.showSingleInputDialog(MainActivity.this,
                    "Send Broadcast Message",
                    ////"Insert the message to send broadcast message.\nThe network name is: " + serviceName,
                    "Message all connected devices in mesh network.",
                    "Enter message here.",
                    publishMsgInput);
        }
    }

//    private void processNewServiceSelection(final IServiceAction serviceAction) {
//        AlertDialogUtils.ISingleInputDialog newServiceInput = new AlertDialogUtils.ISingleInputDialog() {
//
//            @Override
//            public void onOk(String input) {
//                String serviceName = processUserServiceNameInput(input);
//                uiData.addAvailableService(MainActivity.this, serviceName);
//                uiData.addUnsubscribedService(MainActivity.this, serviceName);
//                serviceAction.action(serviceName);
//            }
//
//            @Override
//            public void onCancel() {}
//        };
//
//        AlertDialogUtils.showSingleInputDialog(MainActivity.this,
//                "New Service",
//                "Specify new service",
//                "service",
//                newServiceInput);
//    }

    //////////////////////////////////////////////////////////////////////////////
    // Utilities
    //////////////////////////////////////////////////////////////////////////////

    private boolean isHypeSdkReady() {
        if (!hypeSdk.hasHypeFailed && !hypeSdk.hasHypeStopped && hypeSdk.hasHypeStarted) {
            return true;
        }
        return false;
    }

    private void showHypeNotReadyDialog() {
        if (hypeSdk.hasHypeFailed) {
            AlertDialogUtils.showInfoDialog(MainActivity.this,
                    "Error", "Hype SDK could not be started.\n" + hypeSdk.hypeFailedMsg);
        } else if (hypeSdk.hasHypeStopped) {
            AlertDialogUtils.showInfoDialog(MainActivity.this,
                    "Error", "Hype SDK stopped.\n" + hypeSdk.hypeStoppedMsg);
        } else if (!hypeSdk.hasHypeStarted) {
            AlertDialogUtils.showInfoDialog(MainActivity.this,
                    "Warning", "Hype SDK is not ready yet.");
        }
    }

    private boolean isNoServiceSubscribed() {
        return uiData.getNumberOfSubscribedServices() == 0;
    }

    private void showNoServicesSubscribedDialog() {
        AlertDialogUtils.showInfoDialog(MainActivity.this,
                "INFO", "No services subscribed");

    }

    static String processUserServiceNameInput(String input) {
        return input.toLowerCase().trim();
    }

    //////////////////////////////////////////////////////////////////////////////
    // First Responder activity (shake detection)
    //////////////////////////////////////////////////////////////////////////////


    public void onDialogMajor(DialogFragment dialog) {//dialog response
        sendinfo("Major");
    }

    public void onDialogMinor(DialogFragment dialog) {
        sendinfo("Minor");
    }

    public void onDialogNone(DialogFragment dialog) {
        sendinfo("Uninjured");
    }


    public void requestHypeRequiredPermissions(Activity activity) {

        // Request AccessCoarseLocation permissions if the Android version of the device
        // requires it. Otherwise it starts the Hype SDK immediately. If the permissions are
        // requested the framework only starts if the permissions are granted (see
        // MainActivity.onRequestPermissionsResult()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            activity.requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    HpsConstants.REQUEST_ACCESS_COARSE_LOCATION_ID);
        } else {
            HypeSdkInterface hypeSdkInterface = HypeSdkInterface.getInstance();
            hypeSdkInterface.requestHypeToStart(getApplicationContext());
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
        switch (requestCode) {
            case HpsConstants.REQUEST_ACCESS_COARSE_LOCATION_ID:
                // If the permission is not granted the Hype SDK starts but BLE transport
                // will not be active. Regardless of this, this callback must be implemented
                // because the Hype SDK should only start after the permission request being
                // concluded.
                HypeSdkInterface hypeSdkInterface = HypeSdkInterface.getInstance();
                hypeSdkInterface.requestHypeToStart(getApplicationContext());
                break;
        }
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> list) {
        // Some permissions have been granted
        // ...
    }

    @AfterPermissionGranted(REQUEST_LOCATION_PERMISSION)
    public void requestLocationPermission() {
        String[] perms = {Manifest.permission.ACCESS_FINE_LOCATION};
        if (EasyPermissions.hasPermissions(this, perms)) {
            Toast.makeText(this, "Permission already granted", Toast.LENGTH_SHORT).show();
        } else {
            EasyPermissions.requestPermissions(this, "Please grant the location permission", REQUEST_LOCATION_PERMISSION, perms);
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> list) {
        // Some permissions have been denied
        // ...
    }

    public void getrecords() {

        String url = "https://20191123t191139-dot-eco-emissary-238303.appspot.com/api/record";

        RequestQueue queue = Volley.newRequestQueue(this);

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.

                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            //   Toast.makeText(mycontext,"Response:" + jsonArray,Toast.LENGTH_LONG).show();
                            RecordsReceived = jsonArray;
                        } catch (JSONException e) {
                            Log.e("MYAPP", "unexpected JSON exception", e);
                        }
                        new Handler().postDelayed(new Runnable() { //rerun method after 60 seconds
                            public void run() {
                                getrecords();
                            }
                        }, 10000);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Log.e("Error" + error);
            }
        });

// Add the request to the RequestQueue.
        queue.add(stringRequest);

    }

    public void sendinfo(String condition) {
        shakeRecord.condition = condition;

        final ShakeRecord localdbrecord = shakeRecord;
        String url = "https://20191123t191139-dot-eco-emissary-238303.appspot.com/api/record";
        try {
            jsonBody = new JSONObject("{\"VirtualId\":" + shakeRecord.getVirtualId() + ",\"condition\":" + condition + ",\"gforce\":" + shakeRecord.getGforce() + ",\"Xaxis\":\"" + shakeRecord.getXaxis() + "\",\"Yaxis\":\" " + shakeRecord.getYaxis() + "\",\"Zaxis\":\"" + shakeRecord.getZaxis() + "\",\"longitude\":\"" + shakeRecord.getLongitude() + "\",\"latitude\":\"" + shakeRecord.getLatitude() + "\",\"date\":\"" + shakeRecord.getDate() + "\"}");
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url, jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("TAG", response.toString());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) { // no internet store record locally
                Log.e("TAG", error.getMessage(), error);
                String bla = error.getMessage();
               /* LocalDatabase db = Room.databaseBuilder(getApplicationContext(),
                        LocalDatabase.class, "mylocaldatabase").build();*/

                // LocalDatabase db = LocalDatabase.getAppDatabase(getContext());


                if(bla == null){
                    return;
                }
                try {
                    SharedPreferences sharedPref = getSharedPreferences("Earthquake_Gaurdian_lastshakerecord", Context.MODE_PRIVATE);
                    final SharedPreferences.Editor editor = sharedPref.edit();

                    editor.putString("Id", localdbrecord.getId());
                    editor.putString("VirtualId", localdbrecord.getVirtualId());
                    editor.putString("condition", localdbrecord.getCondition());
                    editor.putString("gforce", localdbrecord.getGforce() + "");
                    editor.putString("Xaxis", localdbrecord.getXaxis() + "");
                    editor.putString("Yaxis", localdbrecord.getYaxis() + "");
                    editor.putString("Zaxis", localdbrecord.getZaxis() + "");
                    editor.putString("longitude", localdbrecord.getLongitude() + "");
                    editor.putString("latitude", localdbrecord.getLatitude() + "");
                    editor.putString("date", localdbrecord.getDate());
                    editor.putString("serverdate", localdbrecord.getServerdate());


                    editor.commit();
                    Toast.makeText(mycontext, "Stored shake event locally", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Log.e("TAG", e.toString());
                }


            }
        }) { //no semicolon or coma
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");

                return params;
            }
        };
        //  mQueue.add(jsonObjectRequest);
        queue.add(jsonObjectRequest);
    }

    private void initLocationService() {

        if (Build.VERSION.SDK_INT >= 23 &&
                ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

        }

        try {
            this.locationManager = (LocationManager) this.getSystemService(this.LOCATION_SERVICE);

            // Get GPS and network status
            this.isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            this.isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isNetworkEnabled && !isGPSEnabled) {
                // cannot get location
                this.locationServiceAvailable = false;
            }

            this.locationServiceAvailable = true;

            if (isNetworkEnabled) {
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                        MIN_TIME_BW_UPDATES,
                        MIN_DISTANCE_CHANGE_FOR_UPDATES, (LocationListener) this);
                if (locationManager != null) {
                    location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

                }
            }

            if (isGPSEnabled) {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                        MIN_TIME_BW_UPDATES,
                        MIN_DISTANCE_CHANGE_FOR_UPDATES, (LocationListener) this);

                if (locationManager != null) {
                    location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                }
            }
        } catch (Exception ex) {
            Log.e(TAG, ex.getMessage());

        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(mShakeDetector, mAccelerometer, SensorManager.SENSOR_DELAY_UI);
        getrecords();
        initLocationService();
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }


}
