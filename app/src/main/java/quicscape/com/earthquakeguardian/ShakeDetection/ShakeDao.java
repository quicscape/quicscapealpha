package quicscape.com.earthquakeguardian.ShakeDetection;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface ShakeDao {


    @Query("SELECT * FROM ShakeRecordTable")
    List<ShakeRecord> getAll();

  /*  @Query("SELECT * FROM ShakeRecordTable WHERE Id IN (:userIds)")
    List<ShakeRecord> loadAllByIds(int[] userIds);*/

   /* @Query("SELECT * FROM ShakeRecordTable WHERE first_name LIKE :first AND " +
            "last_name LIKE :last LIMIT 1")
    ShakeRecord findByName(String first, String last);*/

    @Insert
    void insertrecord(ShakeRecord record);
    @Insert
    void insertAll(ShakeRecord... shakeRecords);

    @Delete
    void delete(ShakeRecord shakeRecords);

}
