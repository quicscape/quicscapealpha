package quicscape.com.earthquakeguardian.ShakeDetection;
// Modified By Joo, Ahmed
//Class that displays the alert dialog when a sudden movement happens.
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;

public class DialogCreator extends DialogFragment {
    private String[] Choices = {"Major Injury", "Minor Injury", "Not Injured"};

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Are You Injured ?")
                .setItems(Choices, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        switch (which) {
                            case 0:
                                listener.onDialogMajor(DialogCreator.this);
                                break;
                            case 1:
                                listener.onDialogMinor(DialogCreator.this);
                                break;
                            case 2:
                                listener.onDialogNone(DialogCreator.this);
                                break;
                        }
                    }
                });
        return builder.create();
    }


    public interface NoticeDialogListener {//listener functions

        public void onDialogMajor(DialogFragment dialog);

        public void onDialogMinor(DialogFragment dialog);

        public void onDialogNone(DialogFragment dialog);
    }

    // Use this instance of the interface to deliver action events
    NoticeDialogListener listener;

    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    @Override
    public void onAttach(Context context) {// Listner that returns data to activity
        super.onAttach(context);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            listener = (NoticeDialogListener) context;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException();
        }
    }


}
