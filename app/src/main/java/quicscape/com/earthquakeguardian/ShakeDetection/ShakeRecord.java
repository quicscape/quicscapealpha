package quicscape.com.earthquakeguardian.ShakeDetection;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Ahmed
 * ShakeRecord class that stores all the shake data and the device data.
 */
@Entity (tableName = "ShakeRecordTable")
public class ShakeRecord {

    @PrimaryKey(autoGenerate = true)
    public int localdbid;
    @NonNull
    public String Id;
    @ColumnInfo(name = "Device_Id")
    public String VirtualId;
    @ColumnInfo(name = "Condition")
    public String condition;
    @ColumnInfo(name = "Gforce")
    public double gforce;
    public double Xaxis;
    public double Yaxis;
    public double Zaxis;
    @ColumnInfo(name = "Longitude")
    public double longitude;
    @ColumnInfo(name = "Latitude")
    public double latitude;
    @ColumnInfo(name = "Device Date")
    public String date;
    public String serverdate;

    public ShakeRecord(String Id, String VirtualId, String condition, double gforce, double Xaxis, double Yaxis, double Zaxis, double longitude, double latitude, String date, String serverdate) {
        this.Id = Id;
        this.VirtualId = VirtualId;
        this.condition = condition;
        this.gforce = gforce;
        this.Xaxis = Xaxis;
        this.Yaxis = Yaxis;
        this.Zaxis = Zaxis;
        this.longitude = longitude;
        this.latitude = latitude;
        this.date = date;
        this.serverdate = serverdate;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getVirtualId() {
        return VirtualId;
    }

    public void setVirtualId(String virtualId) {
        VirtualId = virtualId;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public double getGforce() {
        return gforce;
    }

    public void setGforce(double gforce) {
        this.gforce = gforce;
    }

    public double getXaxis() {
        return Xaxis;
    }

    public void setXaxis(double xaxis) {
        Xaxis = xaxis;
    }

    public double getYaxis() {
        return Yaxis;
    }

    public void setYaxis(double yaxis) {
        Yaxis = yaxis;
    }

    public double getZaxis() {
        return Zaxis;
    }

    public void setZaxis(double zaxis) {
        Zaxis = zaxis;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getServerdate() {
        return serverdate;
    }

    public void setServerdate(String serverdate) {
        this.serverdate = serverdate;
    }
}
