package quicscape.com.earthquakeguardian.ShakeDetection;
/**
 * Ahmed
 * the first responder activity that lists all the data from the api and can navigate to the map activity from her.
 */

import android.Manifest;
import android.app.LauncherActivity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.JsonArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import quicscape.com.earthquakeguardian.R;


import static quicscape.com.earthquakeguardian.MainActivity.getContext;

public class ResponderActivity extends AppCompatActivity implements LocationListener {
    MyRecyclerViewAdapter adapter;
    JSONArray RecordsArray;
    ArrayList<String> RecordsList = new ArrayList<>();
    private ArrayList<ShakeRecord> listofshakerecords = new ArrayList<ShakeRecord>();
    private ArrayList<ShakeRecord> listofshakerecordssortedbyradius = new ArrayList<ShakeRecord>();

    private FusedLocationProviderClient client;

    ImageButton mapButton;
    String Records;
    JSONArray RecordsReceived;
    Context mycontext = this;
    boolean firsttime = true;
    private Spinner sortSpinner;


    Location location;
    private LocationManager locationManager;
    boolean isGPSEnabled;
    boolean isNetworkEnabled;
    boolean locationServiceAvailable;


    final static String TAG = "ResponderActivity";
    private final int REQUEST_LOCATION_PERMISSION = 3;

    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 0; // 10 meters
    private static final long MIN_TIME_BW_UPDATES = 0;//1000 * 60 * 1; // 1 minute


    private double currentUserLatitude;
    private double currentUserLongitude;

    private double civilianLatitude;
    private double civilianLongitude;


    private double userLong;
    private double userLat;

    float distancebetween = 0;

    String[] categories = {"Date(Old-New)", "Date(New-Old)", "Injury(Major)", "Injury(Uninjured)", "Radius < 10km"};

    LocalDatabase db = LocalDatabase.getAppDatabase(getContext());
    final List<ShakeRecord> mylist = db.shakeDao().getAll();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_responder);


//        // add local db records
//        LocalDatabase db = LocalDatabase.getAppDatabase(getContext());
//
//        try {
//            List<ShakeRecord> mylist = db.shakeDao().getAll();
//            if(mylist.size() > 0){
//                for(int myi = 0 ; myi < mylist.size(); myi++){
//                    listofshakerecords.add(mylist.get(myi));
//
//
//                }
//                Toast.makeText(getContext(),"Added offline shake records to list " + mylist.size(),Toast.LENGTH_LONG).show();
//
//            }
//
//        }
//        catch (Exception e){
//            Log.e("TAG", e.toString());
//        }


        initLocationService();
        currentUserLatitude = location.getLatitude();
        currentUserLongitude = location.getLongitude();

        final RecyclerView recyclerView = findViewById(R.id.RecordRv);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));





        //     Toast.makeText(mycontext, "" + currentUserLatitude + "km", Toast.LENGTH_LONG).show();

        //Setting up the spinner for filter the list
        sortSpinner = findViewById(R.id.spinner_sort);
        sortSpinner.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, categories));
        sortSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                //Date(Old-New)
                if (position == 0) {
                    adapter = new MyRecyclerViewAdapter(mycontext, listofshakerecords, location);
                    recyclerView.setAdapter(adapter);
                    Collections.sort(listofshakerecords, new sortByDate());
                    Toast.makeText(mycontext, "Sorted by oldest to newest", Toast.LENGTH_LONG).show();
                } else if (position == 1) {
                    adapter = new MyRecyclerViewAdapter(mycontext, listofshakerecords, location);
                    recyclerView.setAdapter(adapter);
                    Collections.sort(listofshakerecords, new sortByDate());
                    Collections.reverse(listofshakerecords);
                    Toast.makeText(mycontext, "Sorted by Newest to oldest", Toast.LENGTH_LONG).show();

                } else if (position == 2) {
                    adapter = new MyRecyclerViewAdapter(mycontext, listofshakerecords,location);
                    recyclerView.setAdapter(adapter);
                    Collections.sort(listofshakerecords, new sortByDate());
                    Collections.reverse(listofshakerecords);
                    Collections.sort(listofshakerecords, new sortByInjurybyMajor());
                    Toast.makeText(mycontext, "Sorted by Major to Uninjured", Toast.LENGTH_LONG).show();
                } else if (position == 3) {
                    adapter = new MyRecyclerViewAdapter(mycontext, listofshakerecords,location);
                    recyclerView.setAdapter(adapter);
                    Collections.sort(listofshakerecords, new sortByDate());
                    Collections.sort(listofshakerecords, new sortByInjurybyMajor());
                    Collections.reverse(listofshakerecords);
                    Toast.makeText(mycontext, "Sorted by Uninjured to Major", Toast.LENGTH_LONG).show();
                } else {
                    adapter = new MyRecyclerViewAdapter(mycontext, listofshakerecordssortedbyradius, location);
                    recyclerView.setAdapter(adapter);
                    Toast.makeText(mycontext, "Sorted by radius within 10km from your location", Toast.LENGTH_LONG).show();
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        mapButton = findViewById(R.id.mapButton);
        setTitle("Responder Event List");

        //Mapbutton to show the user location on google map
        mapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                opencoordsmap();
            }
        });

        getrecords();


        adapter = new MyRecyclerViewAdapter(this, listofshakerecords, location);


        //Click events for each row.
        adapter.setClickListener(new MyRecyclerViewAdapter.ClickListener() {
            @Override
            public void onItemClick(double Long, double Lat, int position, View v) {
                userLong = Long;
                userLat = Lat;
                Toast.makeText(mycontext, position + 1 + ". You have selected the user where location is: Latitude: " + Lat + ", Longitude: " + Long, Toast.LENGTH_LONG).show();
                openMapFromList();
            }
        });

        //adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);


    }


    public void getrecords() { // calls the api to retrieve stored data

        String url = "https://20191123t191139-dot-eco-emissary-238303.appspot.com/api/record";

        RequestQueue queue = Volley.newRequestQueue(this);

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            //jsonArray = new JSONArray();
                           // Toast.makeText(mycontext,"Response:" + jsonArray.get(0),Toast.LENGTH_LONG).show();
                            for(int i = 0 ; i < mylist.size(); i++){

                                ShakeRecord offlinerecord = mylist.get(i);
                                JSONObject record = new JSONObject();
                                try {
                                    record.put("id", offlinerecord.getId());
                                    record.put("virtualId", offlinerecord.getVirtualId());
                                    record.put("condition", offlinerecord.getCondition());
                                    record.put("gforce", offlinerecord.getGforce());

                                    record.put("xaxis", offlinerecord.getXaxis());
                                    record.put("yaxis", offlinerecord.getYaxis());
                                    record.put("zaxis", offlinerecord.getZaxis());
                                    record.put("longitude", offlinerecord.getLongitude());
                                    record.put("latitude", offlinerecord.getLatitude());
                                    record.put("date", offlinerecord.getDate());
                                    record.put("serverdate", "Offline");
                                    jsonArray.put(record);

                                } catch (JSONException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                               // jsonArray.put()
                            }
                            //
                            try {
                                if (jsonArray.length() > RecordsReceived.length()) {
                                    int difference = jsonArray.length() - RecordsReceived.length();
                                    Toast.makeText(mycontext, difference + " New Events Detected", Toast.LENGTH_LONG).show();
                                }
                            } catch (Exception e) {
                                Log.e("MYAPP", e.toString());
                            }
                            RecordsReceived = jsonArray;
                            updateview();
                        } catch (JSONException e) {
                            Log.e("MYAPP", "unexpected JSON exception", e);
                        }
                        new Handler().postDelayed(new Runnable() { //rerun method after 60 seconds
                            public void run() {
                                getrecords();
                            }
                        }, 10000);// keep checking every 10 seconds
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) { // no internet
                // Log.e("Error" + error);
            }
        });

// Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    private void initLocationService() {

        if (Build.VERSION.SDK_INT >= 23 &&
                ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
        }

        try {
            this.locationManager = (LocationManager) this.getSystemService(this.LOCATION_SERVICE);

            // Get GPS and network status
            this.isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            this.isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isNetworkEnabled && !isGPSEnabled) {
                // cannot get location
                this.locationServiceAvailable = false;
            }

            this.locationServiceAvailable = true;

            if (isNetworkEnabled) {
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                        MIN_TIME_BW_UPDATES,
                        MIN_DISTANCE_CHANGE_FOR_UPDATES, (LocationListener) this);
                if (locationManager != null) {
                    location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

                }
            }

            if (isGPSEnabled) {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                        MIN_TIME_BW_UPDATES,
                        MIN_DISTANCE_CHANGE_FOR_UPDATES, (LocationListener) this);

                if (locationManager != null) {
                    location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                }
            }
        } catch (Exception ex) {
            Log.e(TAG, ex.getMessage());

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        initLocationService();

    }


    public void opencoordsmap() {
        Intent intent = new Intent(this, EventsMap.class);
        // Intent intent = new Intent(this, EventsMap.class);
        // EditText editText = (EditText) findViewById(R.id.editText);
        // String message = editText.getText().toString();
        // intent.putExtra(EXTRA_MESSAGE, message);
        intent.putExtra("recordsarray", Records);
        startActivity(intent);

    }

    public void openMapFromList() {
        Intent intent = new Intent(this, EventsMap.class);
        intent.putExtra("recordsarray", Records);
        intent.putExtra("userLongitude", userLong);
        intent.putExtra("userLatitude", userLat);
        startActivity(intent);

    }

    public void updateview() {
        Records = RecordsReceived.toString();
        try {
            RecordsArray = new JSONArray(Records);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        // data to populate the RecyclerView with

        try {
            //RecordsList.clear();
            // creates a record class from the retreived data and saves it to the local records list then compares whether or not the number of records changed
            if (RecordsList.size() == 0) {
                for (int i = 0; i < RecordsArray.length(); i++) {
                    RecordsList.add(RecordsArray.get(i).toString());
                }
//                for (int i = 0; i < this.RecordsList.size(); i++) {
//                    String Record2 = RecordsList.get(i);
//                    try {
//                        JSONObject obj = new JSONObject(Record2);
//                        ShakeRecord myshakerecord = new ShakeRecord(obj.get("id").toString(), obj.get("virtualId").toString(), obj.get("condition").toString(), Double.parseDouble(obj.get("gforce").toString()), Double.parseDouble(obj.get("xaxis").toString()), Double.parseDouble(obj.get("yaxis").toString()), Double.parseDouble(obj.get("zaxis").toString()), Double.parseDouble(obj.get("longitude").toString()), Double.parseDouble(obj.get("latitude").toString()), obj.get("date").toString(), obj.get("serverdate").toString());
//                        listofshakerecords.add(myshakerecord);
//                    } catch (Exception e) {
//                        Log.e("MYAPP", "unexpected JSON exception", e);
//                    }
                //               }
//                if (listofshakerecords != null) {
//                    Collections.sort(listofshakerecords, new shakerecordcomparer());
//                }
                //   adapter.notifyItemInserted(RecordsList.size() - 1);
            } else {
                int difference = RecordsArray.length() - RecordsList.size();
                int loopstart = RecordsArray.length() - difference;
                if (difference != 0) {
                    for (int ii = loopstart; ii < RecordsArray.length(); ii++) {
                        RecordsList.add(RecordsArray.get(ii).toString());
                    }

                    for (int i = loopstart; i < this.RecordsList.size(); i++) {
                        String Record2 = RecordsList.get(i);
                        try {
                            JSONObject obj = new JSONObject(Record2);
                            ShakeRecord myshakerecord = new ShakeRecord(obj.get("id").toString(), obj.get("virtualId").toString(), obj.get("condition").toString(), Double.parseDouble(obj.get("gforce").toString()), Double.parseDouble(obj.get("xaxis").toString()), Double.parseDouble(obj.get("yaxis").toString()), Double.parseDouble(obj.get("zaxis").toString()), Double.parseDouble(obj.get("longitude").toString()), Double.parseDouble(obj.get("latitude").toString()), obj.get("date").toString(), obj.get("serverdate").toString());
                            Location currentUserLocation = new Location("currentUserLocation");
                            currentUserLocation.setLatitude(currentUserLatitude);
                            currentUserLocation.setLongitude(currentUserLongitude);

                            civilianLatitude = myshakerecord.getLatitude();
                            civilianLongitude = myshakerecord.getLongitude();
                            Location civilianLocation = new Location("civilianLocation");
                            civilianLocation.setLatitude(civilianLatitude);
                            civilianLocation.setLongitude(civilianLongitude);

                            float distancebetween = 0;

                            // Get the distance in km.
                            distancebetween = currentUserLocation.distanceTo(civilianLocation) / 1000;

                            //Toast.makeText(mycontext, "" + distancebetween + "km", Toast.LENGTH_LONG).show();
                            if (distancebetween < 10 && distancebetween > 0) {
                                listofshakerecordssortedbyradius.add(myshakerecord);
                            } else {
                                //Do not add the records.
                            }
                            listofshakerecords.add(myshakerecord);

                        } catch (Exception e) {
                            Log.e("MYAPP", "unexpected JSON exception", e);
                        }

                    }
//                    if (listofshakerecords != null) {
//                        Collections.sort(listofshakerecords, new shakerecordcomparer());
//                    }
                    adapter.notifyItemInserted(RecordsList.size() - 1);
                }
            }
            if (firsttime) {

                for (int i = 0; i < this.RecordsList.size(); i++) {
                    String Record2 = RecordsList.get(i);
                    try {
                        JSONObject obj = new JSONObject(Record2);
                        ShakeRecord myshakerecord = new ShakeRecord(obj.get("id").toString(), obj.get("virtualId").toString(), obj.get("condition").toString(), Double.parseDouble(obj.get("gforce").toString()), Double.parseDouble(obj.get("xaxis").toString()), Double.parseDouble(obj.get("yaxis").toString()), Double.parseDouble(obj.get("zaxis").toString()), Double.parseDouble(obj.get("longitude").toString()), Double.parseDouble(obj.get("latitude").toString()), obj.get("date").toString(), obj.get("serverdate").toString());

                        Location currentUserLocation = new Location("currentUserLocation");
                        currentUserLocation.setLatitude(currentUserLatitude);
                        currentUserLocation.setLongitude(currentUserLongitude);

                        civilianLatitude = myshakerecord.getLatitude();
                        civilianLongitude = myshakerecord.getLongitude();
                        Location civilianLocation = new Location("civilianLocation");
                        civilianLocation.setLatitude(civilianLatitude);
                        civilianLocation.setLongitude(civilianLongitude);


                        distancebetween = currentUserLocation.distanceTo(civilianLocation) / 1000; // in km
                     //    Toast.makeText(mycontext, "" + distancebetween + "km", Toast.LENGTH_LONG).show();
                        if (distancebetween <= 10 && distancebetween > 0) {
                            listofshakerecordssortedbyradius.add(myshakerecord);
                        } else {
                            //Do not add the records.
                        }
                        listofshakerecords.add(myshakerecord);
                    } catch (Exception e) {
                        Log.e("MYAPP", "unexpected JSON exception", e);
                    }

                }

                adapter.notifyItemInserted(RecordsList.size() - 1);
                firsttime = false;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        // set up the RecyclerView

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public class sortByDate implements Comparator<ShakeRecord> {
        public int compare(ShakeRecord left, ShakeRecord right) {

            return left.getDate().compareTo(right.getDate());
        }
    }


    public class sortByInjurybyMajor implements Comparator<ShakeRecord> {
        public int compare(ShakeRecord left, ShakeRecord right) {


            return left.condition.compareTo(right.condition);

            //return left.getDate().compareTo(right.getDate());
        }
    }


}
