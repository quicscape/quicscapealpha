package quicscape.com.earthquakeguardian.ShakeDetection;
/**
 * A class that inflates the api data in a list for the first responder.
 */

import android.content.Context;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import kotlin.text.Regex;
import quicscape.com.earthquakeguardian.R;

import static quicscape.com.earthquakeguardian.MainActivity.getContext;

public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.ViewHolder> {

    private ArrayList<ShakeRecord> mData;
    private LayoutInflater mInflater;
    private static ClickListener clickListener;
    private int injuryID;

    //   private String city;
    //  private String country;
    private String address;
    private double userLongitude;
    private double userLatitude;
    private String date;
    private String time;
    private String dateTime;
    ArrayList<Float> listofdistances ;
    ArrayList<ArrayList> mylist ;
    private Location mycurrentlocation;

    // data is passed into the constructor
    MyRecyclerViewAdapter(Context context, ArrayList<ShakeRecord> data , Location currentlocation) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.mycurrentlocation = currentlocation;

    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.my_text_view, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ShakeRecord userShakeRecord = mData.get(position);


        //Setting up the geocoder for getting user location based on longitiude and latitude
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(getContext(), Locale.getDefault());


        //Get the background color of circle that is represented of injury state
        GradientDrawable magnitudeCircle = (GradientDrawable) holder.injuryViewColor.getBackground();

        try {
            // this instead of the line below it ok why? cause i changed it into a list of objects instead of list of json
            if (userShakeRecord.condition.contains("Major")) {
                injuryID = 3;
                holder.userinjuryState.setTextColor(Color.parseColor("#D93218"));
            } else if (userShakeRecord.condition.contains("Minor")) {
                injuryID = 2;
                holder.userinjuryState.setTextColor(Color.parseColor("#F5A623"));
            } else {
                //Uninjured
                injuryID = 1;
                holder.userinjuryState.setTextColor(Color.parseColor("#04B4B3"));
            }

            //change to circle color depends on injury state
            int magnitudeColor = getMagnitudeColor(injuryID);
            magnitudeCircle.setColor(magnitudeColor);

            Location currentUserLocation = new Location("currentUserLocation");
            currentUserLocation.setLatitude(mycurrentlocation.getLatitude());
            currentUserLocation.setLongitude(mycurrentlocation.getLongitude());


            userLatitude = userShakeRecord.getLatitude();
            userLongitude = userShakeRecord.getLongitude();
            Location civilianLocation = new Location("civilianLocation");
            civilianLocation.setLatitude(userLatitude);
            civilianLocation.setLongitude(userLongitude);
            double distancebetween = 0;
            distancebetween = currentUserLocation.distanceTo(civilianLocation) / 1000; // in km


            try {
                addresses = geocoder.getFromLocation(userLatitude, userLongitude, 1);
                if (userLongitude == 0 || userLatitude == 0) {//city = "Location Unknown";
                    address = "Address Unknown";
                } else {
                    //  city = addresses.get(0).getLocality();
                    // country = addresses.get(0).getCountryName();
                    address = addresses.get(0).getAddressLine(0);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            //split the date and time to make better format
            dateTime = userShakeRecord.getServerdate();
            if(!dateTime.equals("Offline")) {


                String[] spliter = dateTime.split(" ");
                date = spliter[0];
                time = spliter[1];
                holder.recordDate.setText("" + date);
                holder.recordTime.setText("" + time);
            }
            else{
                dateTime = "Offline";
                holder.recordDate.setText(dateTime);
            }


            holder.distance = distancebetween;

            holder.userinjuryState.setText("Injury condition - " + userShakeRecord.getCondition());
            holder.userLocation.setText(position + 1 + ". Location: " + address + ".");
           // holder.deviceID.setText(String.format("Latitude: " + "%.2f" + ", Longitude: " + "%.2f", userLatitude, userLongitude));
            holder.deviceID.setText(String.format("This civilian is " + "%.2f"+  "km away", holder.distance ));

            holder.mylat = userLatitude;
            holder.mylong = userLongitude;


        } catch (Exception e) {
            Log.e("MYAPP", "unexpected JSON exception", e);
        }

    }

    private int getMagnitudeColor(int injury) {

        int injuryStateId = 0;
        int civilianInjury = injury;
        switch (civilianInjury) {
            case 0:
            case 1:
                injuryStateId = R.color.magnitudeNotInjured;
                break;
            case 2:
                injuryStateId = R.color.magnitudeMinorInjured;
                break;
            case 3:
                injuryStateId = R.color.magnitudeMajorInjured;
                break;
        }
        return ContextCompat.getColor(getContext(), injuryStateId);
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView deviceID;
        TextView userLocation;
        TextView userinjuryState;
        TextView recordDate;
        TextView recordTime;
        TextView injuryViewColor;
        double mylat;
        double mylong;
        double distance;
        float myloc;



        ViewHolder(View itemView) {
            super(itemView);
            userLocation = itemView.findViewById(R.id.locationRecord);
            deviceID = itemView.findViewById(R.id.deviceIDRecord);
            userinjuryState = itemView.findViewById(R.id.injuryState);
            recordDate = itemView.findViewById(R.id.date);
            recordTime = itemView.findViewById(R.id.time);
            injuryViewColor = itemView.findViewById(R.id.injuryView);
            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            if (clickListener != null) {
                clickListener.onItemClick(mylong, mylat, getAdapterPosition(), view);
            }
        }
    }


    // convenience method for getting data at click position
    String getItem(int id) {
        return mData.get(id).toString();
    }

    // allows clicks events to be caught
    void setClickListener(ClickListener clickListener) {
        MyRecyclerViewAdapter.clickListener = clickListener;
    }


    // parent activity will implement this method to respond to click events
    public interface ClickListener {
        void onItemClick(double Long, double Lat, int position, View v);
    }


}