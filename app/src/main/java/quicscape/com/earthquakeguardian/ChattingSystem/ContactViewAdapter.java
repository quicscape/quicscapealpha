//
// MIT License
//
// Copyright (C) 2018 HypeLabs Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

package quicscape.com.earthquakeguardian.ChattingSystem;
//Modified by Ahmed
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.hypelabs.hype.Message;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

import quicscape.com.earthquakeguardian.R;
import quicscape.com.earthquakeguardian.ShakeDetection.LocalDatabase;
import quicscape.com.earthquakeguardian.ShakeDetection.ShakeRecord;

//Adapter for Contact Activity to display connected devices and display battery info
public class ContactViewAdapter extends BaseAdapter {

    private Context context;
    private Map<String, Store> stores;
    private LayoutInflater inflater = null;
    private View.OnTouchListener onTouchListener;

    public ContactViewAdapter(Context context, Map<String, Store> stores, View.OnTouchListener onTouchListener) {

        this.context = context;
        this.stores = stores;
        this.onTouchListener = onTouchListener;
    }

    protected LayoutInflater getInflater() {

        if (inflater == null) {
            inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        return inflater;
    }

    protected Context getContext() {

        return context;
    }

    protected Map<String, Store> getStores() {

        return stores;
    }

    @Override
    public int getCount() {

        return stores.size();
    }

    @Override
    public Object getItem(int position) {

        // This way of getting ordered stores is an overkill, but that's not the point of this demo
        return getStores().values().toArray()[position];
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View vi = convertView;

        if (vi == null)
            vi = getInflater().inflate(R.layout.contact_cell_view, null);

        Store store = (Store) getItem(position);

        boolean batterylifefound = false;
        boolean civilianshakefound = false;
        TextView displayName = (TextView)vi.findViewById(R.id.hype_id);
        TextView announcement = (TextView)vi.findViewById(R.id.hype_announcement);
        TextView batterylevel = (TextView)vi.findViewById(R.id.battery_level);
        ImageView contentIndicator = (ImageView) vi.findViewById(R.id.new_content);

        displayName.setText(store.getInstance().getStringIdentifier());
        //Filter through store data a display devices that sent their battery info
        if(store.getMessages().size() > 0) {
            for (int i = store.getMessages().size(); i-- > 0; ) {
                if(!batterylifefound || !civilianshakefound) {
                    final Message message2 = store.getMessages().get(i);
                    try {
                        String dataMessage = new String(message2.getData(), "UTF-8");
                        JsonObject jsonObject = new JsonParser().parse(dataMessage).getAsJsonObject();
                        if (!dataMessage.contains("N<C")) {
                            if(!batterylifefound) {

                                if (!ChatApplication.announcement.equals(jsonObject.get("deviceName").getAsString())) {
                                    batterylevel.setText("Battery Level: " + jsonObject.get("batterylife").getAsString() + "%");
                                    batterylifefound = true;
                                }
                            }

                            if(!civilianshakefound && !ChatApplication.announcement.equals(jsonObject.get("deviceName").getAsString())){

                                if (!ChatApplication.announcement.equals(jsonObject.get("deviceName").getAsString() )) {

                                    if(!jsonObject.get("record").getAsString().equals("null")){// get the record from message and get its inner data
                                        String myrecord = jsonObject.get("record").getAsString();
                                        JsonObject shakerecorddata = new JsonParser().parse(myrecord).getAsJsonObject();

                                        String Id = shakerecorddata.get("Id").getAsString();
                                        String VirtualId = shakerecorddata.get("VirtualId").getAsString();
                                        String condition = shakerecorddata.get("condition").getAsString();
                                        double gforce = shakerecorddata.get("gforce").getAsDouble();
                                        double Xaxis = shakerecorddata.get("Xaxis").getAsDouble();
                                        double Yaxis = shakerecorddata.get("Yaxis").getAsDouble();
                                        double Zaxis = shakerecorddata.get("Zaxis").getAsDouble();
                                        double longitude = shakerecorddata.get("longitude").getAsDouble();
                                        double latitude = shakerecorddata.get("latitude").getAsDouble();
                                        String date = shakerecorddata.get("date").toString();
                                        String serverdate = shakerecorddata.get("serverdate").toString();
                                        ShakeRecord civilianshakerecord = new ShakeRecord(Id,VirtualId,condition,gforce,Xaxis,Yaxis,Zaxis,longitude,latitude,date,serverdate);

                                        LocalDatabase db = LocalDatabase.getAppDatabase(getContext());

                                        try {
                                            db.shakeDao().insertrecord(civilianshakerecord);
                                            Toast.makeText(getContext(),"Recieved civilian text with shake event " + civilianshakerecord.getCondition(),Toast.LENGTH_LONG).show();
                                        }
                                        catch (Exception e){
                                            Log.e("TAG", e.toString());
                                        }
                                    }
                                }

                                civilianshakefound = true;

                            }
                        }
                    } catch (Exception e) {

                    }
                }





            }
            final Message message2 = store.getMessages().get(store.getMessages().size()-1);
            try {
                String dataMessage = new String(message2.getData(), "UTF-8");
                if(!dataMessage.contains("N<C")) {
                    JsonObject jsonObject = new JsonParser().parse(dataMessage).getAsJsonObject();
                    if(!ChatApplication.announcement.equals(jsonObject.get("deviceName").getAsString())) {
                        batterylevel.setText("Battery Level: " + jsonObject.get("batterylife").getAsString() + "%");
                    }

                }
            }
            catch (Exception e){

            }
        }

        try {
            announcement.setText(new String(store.getInstance().getAnnouncement(), "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            announcement.setText("");
        }

        contentIndicator.setVisibility(store.hasNewMessages() ? View.VISIBLE : View.INVISIBLE);

        vi.setOnTouchListener(getOnTouchListener());

        return vi;
    }

    protected View.OnTouchListener getOnTouchListener() {

        return onTouchListener;
    }
}
