//
// MIT License
//
// Copyright (C) 2018 HypeLabs Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

package quicscape.com.earthquakeguardian.ChattingSystem;
//Modified by Kenji,Ahmed
import android.Manifest;
import android.animation.Animator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.BatteryManager;
import android.os.Bundle;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.hypelabs.hype.Hype;
import com.hypelabs.hype.Instance;
import com.hypelabs.hype.Message;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;

import quicscape.com.earthquakeguardian.ARActivity;
import quicscape.com.earthquakeguardian.ChatModel;
import quicscape.com.earthquakeguardian.R;
import quicscape.com.earthquakeguardian.ShakeDetection.ShakeRecord;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

//Activity responsible for one to one chatting
public class ChatActivity extends AppCompatActivity implements Store.Delegate {

    public static String INTENT_EXTRA_STORE = "com.hypelabs.store";

    // Friend Location
    private String friendLon = null;
    private String friendLat = null;

    // My Location
    private LocationManager locationManager;
    private Double myLat = 0.0;
    private Double myLon = 0.0;
    private Location myLocation;
    private static final int PERMISSION_CODE = 1; //Can be any random number
    private String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.CAMERA};// Permission to get picture and location

    private String user;

    // Friend Identifiers
    private String friendUsername;
    private String imageToSend = null;
    private String connectedto = "";

    FloatingActionButton fab, arButton, imgButton, alrmButton;
    LinearLayout fabLayout1, fabLayout2, fabLayout3;
    boolean isFABOpen = false;

    private boolean alarm_bool = false;

    private FusedLocationProviderClient fuseLocationClient;

    protected Message sendMessage(String text, Instance instance) throws UnsupportedEncodingException {

        // When sending content there must be some sort of protocol that both parties
        // understand. In this case, we simply send the text encoded in UTF-8. The data
        // must be decoded when received, using the same encoding.
        byte[] data = text.getBytes("UTF-8");

        // Sends the data and returns the message that has been generated for it. Messages have
        // identifiers that are useful for keeping track of the message's deliverability state.
        // In order to track message delivery set the last parameter to true. Notice that this
        // is not recommend, as it incurs extra overhead on the network. Use this feature only
        // if progress tracking is really necessary.
        return Hype.send(data, instance, false);
    }

    @Override
    protected void onStart() {

        super.onStart();

        setContentView(R.layout.chat_view);
        //Request permission for location and read storage if not granted already
        ActivityCompat.requestPermissions(this, permissions, PERMISSION_CODE);

        fuseLocationClient = LocationServices.getFusedLocationProviderClient(this);

        SharedPreferences sharedPrefUser = getSharedPreferences("Earthquake_Gaurdian_User", Context.MODE_PRIVATE);
        user = sharedPrefUser.getString("User", "[N/A]");

        final ListView listView = (ListView) findViewById(R.id.list_view);
        final FloatingActionButton sendButton = (FloatingActionButton) findViewById(R.id.send_fab);

        final EditText messageField = (EditText) findViewById(R.id.message_field);

        final Store store = getStore();
        //Get GPS location for the device
        setupLocation();

        listView.setAdapter(new ChatViewAdapter(this, store));

        TextView announcement = findViewById(R.id.chatViewInstanceAnnouncement);

        fabLayout1 = (LinearLayout) findViewById(R.id.fabLayout1);
        fabLayout2 = (LinearLayout) findViewById(R.id.fabLayout2);
        fabLayout3 = (LinearLayout) findViewById(R.id.fabLayout3);
        fab = (FloatingActionButton) findViewById(R.id.fab);

        arButton = (FloatingActionButton) findViewById(R.id.ar_fab);
        imgButton = (FloatingActionButton) findViewById(R.id.image_fab);
        alrmButton = (FloatingActionButton) findViewById(R.id.alarm_fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isFABOpen) {
                    showFABMenu(user);
                } else {
                    closeFABMenu();
                }
            }
        });

        try {
            announcement.setText("Connected to " + new String(store.getInstance().getAnnouncement(), "UTF-8"));
            connectedto =  new String(store.getInstance().getAnnouncement(), "UTF-8");
            setTitle("Earthquake Guardian");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        // Start listening to message queue events so we can update the UI accordingly
        store.setDelegate(this);

        // Flag all messages as read
        store.setLastReadIndex(store.getMessages().size());
        //User identifier
        //final SharedPreferences sharedPref = getSharedPreferences("Earthquake_Gaurdian_User", Context.MODE_PRIVATE);

        //Get current dateTime, battery level, message and gps location
        //Convert into JSON and send message using Hype
        sendButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                sendMessage("");
            }
        });

        //Get gps location from latest message in the instance and pass intent to AR view
        arButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Message message = null;
                if (store.getMessages().size() > 0) {
                    int deviceIntLocation = 0;
                    try {

                        for (int i = store.getMessages().size(); i-- > 0; ) {

                            Message messageLocation = (Message) store.getMessages().get(i);
                            try {
                                String dataMessage = new String(messageLocation.getData(), "UTF-8");
                                if (!dataMessage.contains("N<C")) {
                                    JsonObject jsonObject = new JsonParser().parse(dataMessage).getAsJsonObject();
                                    String devicename = jsonObject.get("deviceName").getAsString().replace("[C]", "").replace("[FR]", "");
                                    if (!ChatApplication.announcement.equals(devicename)) {
                                        deviceIntLocation = i;

                                        message = store.getMessages().get(i);
                                        break;
                                    }
                                }
                            } catch (Exception e) {

                            }
                        }


                        if (message == null) {
                            return;
                        }

                        //Message message = (Message) store.getMessages().get(deviceIntLocation);
                        String dataMessage = new String(message.getData(), "UTF-8");
                        if (!dataMessage.contains("N<C")) {
                            JsonObject jsonObject = new JsonParser().parse(dataMessage).getAsJsonObject();
                            friendLat = jsonObject.get("latitude").getAsString();
                            friendLon = jsonObject.get("longitude").getAsString();
                            friendUsername = jsonObject.get("deviceName").getAsString();
                        } else {
                            friendLat = "0.0";
                            friendLon = "0.0";
                        }
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    if (friendLat != null && friendLon != null) {
                        Intent intent = new Intent(getApplicationContext(), ARActivity.class);
                        intent.putExtra("targetLat", friendLat);
                        intent.putExtra("targetLon", friendLon);
                        intent.putExtra("username", friendUsername);
                        //Toast.makeText(getApplicationContext(), friendUsername + " Lat: " + friendLat + " Lon: " + friendLon, Toast.LENGTH_LONG).show();
                        startActivity(intent);
                    } else {
                        Toast.makeText(getApplicationContext(), "Location from friend not yet received. No GPS signal.", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Location from friend not yet received. No GPS signal.", Toast.LENGTH_LONG).show();
                }
            }
        });
        imgButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickFromGallery();
            }
        });

        alrmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (alarm_bool == false) {
                    alarm_bool = true;
                    sendMessage("#playsound");
                } else {
                    alarm_bool = false;
                    sendMessage("#stopsound");
                }
            }
        });
    }

    private void showFABMenu(String user) {
        isFABOpen = true;
        fabLayout1.setVisibility(View.VISIBLE);
        fabLayout2.setVisibility(View.VISIBLE);
        if (user.equals("[FR]")) {
            fabLayout3.setVisibility(View.VISIBLE);
        }
        fab.animate().rotationBy(180);
        fabLayout1.animate().translationY(-getResources().getDimension(R.dimen.standard_65));
        fabLayout2.animate().translationY(-getResources().getDimension(R.dimen.standard_120));
        fabLayout3.animate().translationY(-getResources().getDimension(R.dimen.standard_180));
    }

    private void closeFABMenu() {
        isFABOpen = false;
        fab.animate().rotation(0);
        fabLayout1.animate().translationY(0);
        fabLayout2.animate().translationY(0);
        fabLayout3.animate().translationY(0);
        fabLayout3.animate().translationY(0).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                if (!isFABOpen) {
                    fabLayout1.setVisibility(View.GONE);
                    fabLayout2.setVisibility(View.GONE);
                    fabLayout3.setVisibility(View.GONE);
                }
/*                if (fab.getRotation() != -180) {
                    fab.setRotation(-180);
                }*/
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
    }

    private void sendMessage(String messageString) {

        EditText messageField = (EditText) findViewById(R.id.message_field);
        String msgtosend;

        if (messageString == "#playsound" || messageString == "#stopsound") {
            msgtosend = messageString;
        } else {
            msgtosend = messageField.getText().toString();
        }

        final Context mycontext = this;
        Store store = getStore();

        setupLocation();

        SimpleDateFormat formatter = new SimpleDateFormat("H:mm a");
        Date date = new Date();

        BatteryManager bm = (BatteryManager) getSystemService(BATTERY_SERVICE);//gets current battery level.
        int batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);// assigns it to a variable

        //will get the location from the setuplocation() method and send it to first responder (tbd)

        String batterypercentage = Integer.toString(batLevel);
        //Check the imageToSend is not empty when sending a message

        if (imageToSend != null) {
            msgtosend = "▶";
        }
        //Setup message as a ChatModel and covert to JSON
        ShakeRecord recordtosend = null;
        if (connectedto.contains("[FR]")) {
            SharedPreferences sharedPref = getSharedPreferences("Earthquake_Gaurdian_lastshakerecord", Context.MODE_PRIVATE);

            String Id = sharedPref.getString("Id", "[N/A]");
            String VirtualId = sharedPref.getString("VirtualId", "[N/A]");
            String condition = sharedPref.getString("condition", "[N/A]");
            String gforce = sharedPref.getString("gforce", "[N/A]");
            String Xaxis = sharedPref.getString("Xaxis", "[N/A]");
            String Yaxis = sharedPref.getString("Yaxis", "[N/A]");
            String Zaxis = sharedPref.getString("Zaxis", "[N/A]");
            String longitude = sharedPref.getString("longitude", "[N/A]");
            String latitude = sharedPref.getString("latitude", "[N/A]");
            String mydate = sharedPref.getString("date", "[N/A]");
            String serverdate = sharedPref.getString("serverdate", "[N/A]");

            if (Id != "[N/A]") {
                ShakeRecord storedofflinerecord = new ShakeRecord(Id, VirtualId, condition, Double.valueOf(gforce), Double.valueOf(Xaxis), Double.valueOf(Yaxis), Double.valueOf(Zaxis), Double.valueOf(longitude), Double.valueOf(latitude), mydate, serverdate);
                recordtosend = storedofflinerecord;
                Toast.makeText(mycontext, "Sent local shake event to FR", Toast.LENGTH_SHORT).show();
                sharedPref.edit().clear().commit(); // delete the record after sending it to the fr to avoid sending it again
                deleteSharedPreferences("Earthquake_Gaurdian_lastshakerecord");

                SharedPreferences sharedPref2 = getSharedPreferences("Earthquake_Gaurdian_lastshakerecord", Context.MODE_PRIVATE);
                String myvirtualid = sharedPref.getString("VirtualId", "[N/A]");
                String bla = myvirtualid;

            }
        }
        ChatModel model = new ChatModel(msgtosend, user + ChatApplication.announcement, formatter.format(date), batterypercentage, "", myLon, myLat, imageToSend, recordtosend);
        Gson gson = new Gson();
        String text = gson.toJson(model);
        //Set image to null after sending image
        imageToSend = null;
        // Avoids accidental presses
        if (text == null || text.length() == 0) {
            return;
        }
        //Try to send message
        try {
            Log.v(getClass().getSimpleName(), "send message");
            Message message;
            if(store != null) {
                message = sendMessage(text, store.getInstance());
            }
            else{
                Toast.makeText(mycontext,"Please try to send again !",Toast.LENGTH_SHORT).show();
                return;
            }

            if (message != null) {

                // Clear message content
                messageField.setText("");

                // Add the message to the store so it shows in the UI
                store.add(message);
            }

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    //Get GPS location for the device
    private void setupLocation() {
//        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//        LocationListener locationListenerGPS = new LocationListener() {
//            @Override
//            public void onLocationChanged(Location location) {
//                myLat = location.getLatitude();
//                myLon = location.getLongitude();
//                myLocation = location;
//            }
//
//            @Override
//            public void onStatusChanged(String provider, int status, Bundle extras) {
//
//            }
//
//            @Override
//            public void onProviderEnabled(String provider) {
//
//            }
//
//            @Override
//            public void onProviderDisabled(String provider) {
//
//            }
//        };

        fuseLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            // Logic to handle location object
                            myLat = location.getLatitude();
                            myLon = location.getLongitude();
                            myLocation = location;
                        }
                    }
                });
        //Check for missing permissions
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        //locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 1, locationListenerGPS);
    }

    //Start gallery intent
    private void pickFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1);
    }

    //Load image from gallery
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        FloatingActionButton sendButton = (FloatingActionButton) findViewById(R.id.send_fab);
        if (requestCode == 1 && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri uri = data.getData();
            //Try to get image from gallery
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);

                int nh = (int) (bitmap.getHeight() * (812.0 / bitmap.getWidth()));
                Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 812, nh, true);

                imageToSend = getStringFromBitmap(scaled);
                sendButton.performClick();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //Convert Bitmap into string
    private String getStringFromBitmap(Bitmap bitmapPicture) {
        final int COMPRESSION_QUALITY = 10;
        String encodedImage;
        ByteArrayOutputStream byteArrayBitmapStream = new ByteArrayOutputStream();
        bitmapPicture.compress(Bitmap.CompressFormat.JPEG, COMPRESSION_QUALITY,
                byteArrayBitmapStream);

        byte[] b = byteArrayBitmapStream.toByteArray();
        encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
        return encodedImage;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // We now have permission to use the location
            }
        }
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();

        if (getStore() != null) {
            getStore().setLastReadIndex(getStore().getMessages().size());
        }
    }

    //Update listView when new message is added
    @Override
    public void onMessageAdded(Store store, Message message) {

        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final ListView listView = (ListView) findViewById(R.id.list_view);

                ((ChatViewAdapter) listView.getAdapter()).notifyDataSetChanged();
            }
        });
    }

    protected Store getStore() {// get the unique store required

        ChatApplication chatApplication = (ChatApplication) getApplication();
        String storeIdentifier = getIntent().getStringExtra(INTENT_EXTRA_STORE);
        Store store = chatApplication.getStores().get(storeIdentifier);

        return store;
    }
}