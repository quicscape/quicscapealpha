//
// MIT License
//
// Copyright (C) 2018 HypeLabs Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

package quicscape.com.earthquakeguardian.ChattingSystem;
//Modified by Kenji, Joo
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.hypelabs.hype.Message;

import java.io.UnsupportedEncodingException;
import java.util.List;

import quicscape.com.earthquakeguardian.AudioAlertUtils;
import quicscape.com.earthquakeguardian.R;
import quicscape.com.earthquakeguardian.ShakeDetection.LocalDatabase;
import quicscape.com.earthquakeguardian.ShakeDetection.ShakeRecord;

//Adapter for Chat Activity
public class ChatViewAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater inflater = null;
    private Store store;

    public ChatViewAdapter(Context context, Store store) {

        this.context = context;
        this.store = store;
    }

    protected LayoutInflater getInflater() {

        if (inflater == null) {
            inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        return inflater;
    }

    protected Context getContext() {

        return context;
    }

    protected Store getStore() {

        return store;
    }

    @Override
    public int getCount() {

        return getStore().getMessages().size();
    }

    @Override
    public Object getItem(int position) {

        return getStore().getMessages().get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View vi = convertView;

        if (vi == null) {
            vi = getInflater().inflate(R.layout.chat_cell_view, null);
        }
        final Message message = (Message) getItem(position);

        TextView text = (TextView) vi.findViewById(R.id.text);
        TextView deviceName = (TextView) vi.findViewById(R.id.device_name);
        TextView time = (TextView) vi.findViewById(R.id.time);
        ImageView myimageview = (ImageView) vi.findViewById(R.id.receivedimg);

        //Filter through current instance and display only direct messages
        try {
            //Get list of data
            String dataMessage = new String(message.getData(), "UTF-8");
            //Get latest message from the list
            Message latestMessage = (Message) store.getMessages().lastElement();
            String alertMessage = new String(latestMessage.getData(), "UTF-8");
            //Create AudioAlertUtils class
            AudioAlertUtils soundAlert = new AudioAlertUtils();

            if (!dataMessage.contains("N<C")) {
                myimageview.setImageBitmap(null);
                //Read message for alert sound
                if (alertMessage.contains("#playsound") && !alertMessage.contains(android.os.Build.MODEL) && alertMessage.contains("FR")) {
                    soundAlert.playBeep();
                } else if (alertMessage.contains("#stopsound") && !alertMessage.contains(android.os.Build.MODEL) && alertMessage.contains("FR")) {
                    soundAlert.stopBeep();
                }

                //Parse the messages
                JsonObject jsonObject = new JsonParser().parse(dataMessage).getAsJsonObject();
                if (jsonObject.get("message").getAsString().contains("#playsound")) {
                    text.setText("*Alarm Enabled by First Responder*");
                } else if (jsonObject.get("message").getAsString().contains("#stopsound")) {
                    text.setText("*Alarm Disabled by First Responder*");
                } else {
                    text.setText(jsonObject.get("message").getAsString());
                }

                deviceName.setText(jsonObject.get("deviceName").getAsString());
                time.setText(jsonObject.get("time").getAsString());
                //Try to set image in the chat
                try {
                    String receivedimagestring = jsonObject.get("userImage").getAsString();
                    myimageview.setImageBitmap(getBitmapFromString(receivedimagestring));
                    receivedimagestring = null;
                } catch (Exception e) {

                }
            } else {
                text.setText("*Info Broadcasted Check Notification*");
                deviceName.setText("Emergency Broadcast Channel");
                time.setText(" ");
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return vi;
    }

    //Convert String into Bitmap
    private Bitmap getBitmapFromString(String stringPicture) {
        byte[] decodedString = Base64.decode(stringPicture, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return decodedByte;
    }
}
