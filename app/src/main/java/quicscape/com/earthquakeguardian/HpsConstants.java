package quicscape.com.earthquakeguardian;

//Modified by Kenji
import java.util.ArrayList;
import java.util.Arrays;

// This class is saving constant variables that are used in other classes.
public class HpsConstants {
    static public final String APP_IDENTIFIER = "f1d1f46c";
    static public final String ACCESS_TOKEN = "90338a402e8e0d57";
    static public final String HASH_ALGORITHM = "SHA-1";
    static public final int HASH_ALGORITHM_DIGEST_LENGTH = 20;
    static public final String ENCODING_STANDARD = "UTF-8";
    static public final String NOTIFICATIONS_CHANNEL = "HypePubSub";
    static public final String NOTIFICATIONS_TITLE = "Emergency Service";
    static public final String LOG_PREFIX = " :: HpsApplication :: ";
    static public final ArrayList<String> STANDARD_HYPE_SERVICES = new ArrayList<>(Arrays.asList(
            "Emergency Broadcast"));
    //    static public final ArrayList<String> STANDARD_HYPE_SERVICES = new ArrayList<>(Arrays.asList(
//            "hype-jobs", "hype-sports", "hype-news", "hype-weather", "hype-music", "hype-movies"));
    static public final int REQUEST_ACCESS_COARSE_LOCATION_ID = 0;
}
