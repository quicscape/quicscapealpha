package quicscape.com.earthquakeguardian;
//Modified by Kenji

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import quicscape.com.earthquakeguardian.WearDemo.WearActivity;

//Simple login page to determine who the current user is
public class LoginPageActivity extends AppCompatActivity {

    ImageButton CivButton, FirstResButton;

    private static final int PERMISSION_CODE = 1; //Can be any random number
    private String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION
            , Manifest.permission.CAMERA};// Permission to get picture and location

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_page);

        ActivityCompat.requestPermissions(this, permissions, PERMISSION_CODE);

        //Setup SharedPreferences to determine who is the user
        SharedPreferences sharedPref = getSharedPreferences("Earthquake_Gaurdian_User", Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedPref.edit();

        editor.putBoolean("joinNet", false);
        editor.commit();

        CivButton = findViewById(R.id.civ_button);
        FirstResButton = findViewById(R.id.firstRes_button);
        setTitle("Earthquake Guardian");

        CivButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putString("User", "[C]");
                editor.commit();
                Intent intent = new Intent(LoginPageActivity.this, MainActivity.class);
                startActivity(intent);
                //finish();
            }
        });

        FirstResButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialogUtils.ISingleInputDialog publishMsgInput = new AlertDialogUtils.ISingleInputDialog() {

                    @Override
                    public void onOk(String password) {
                        password = password.trim();
                        if (password.length() == 0) {
                            AlertDialogUtils.showInfoDialog(LoginPageActivity.this,
                                    "WARNING",
                                    "A valid passcode must be entered");
                            return;
                        }
                        if (password.equals("1234")) {
                            editor.putString("User", "[FR]");
                            editor.commit();
                            Intent intent = new Intent(LoginPageActivity.this, MainActivity.class);
                            startActivity(intent);
                            //finish();
                        }
                        if (password.equals("#weardemo")) {
                            editor.putString("User", "[FR]Wear OS ");
                            editor.commit();
                            Intent intent = new Intent(LoginPageActivity.this, MainActivity.class);
                            startActivity(intent);
                            //finish();
                        }
                    }

                    @Override
                    public void onCancel() {
                    }
                };

                AlertDialogUtils.showSingleInputDialog(LoginPageActivity.this,
                        "First Responder",
                        "Enter a passcode for access",
                        "Number code",
                        publishMsgInput);
            }
        });

    }

    @Override
    protected void onResume() {

        super.onResume();
    }
}
