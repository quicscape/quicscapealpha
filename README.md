# **Earthquake Guardian: Mesh Networking Emergency Response**
  
![Alt text](https://raw.githubusercontent.com/JooHyunPark-JP/JooHyunPark-JP.github.io/master/images/EarthQuakeGuardian.gif)
  
# Getting Started
---
**Please follow this instruction before trying this app...**  
1. Please go to "Downloads" section on the left panel and download "Hype.aar" file  
2. Download the repository folder.  
3. Unzip the folder (Folder name should be quicscapealpha)  
4. Download the Hype.aar file.  
5. Move "Hype.aar" file into "hype" folder which is in quicscapealpha folder. 

# After launching the app...
---
**When you launch the app, there are two ways to log-in in order to access.**  
1. When you click the icon of "First Responder" that means you are trying to log-in as a first responder and Civilian means you are trying to log-in as a Civilian. (Check out the "Roles" below to see what their roles are).   
2. The password for First responder is **"1234"** (for now) however civilian does not required password.    

## ***Important***
### **1. Make sure the Location (GPS) is "ON"**
### **2. Make sure the Bluetooth is "ON" for the best performance**

# Features
---
In **main menu**, there are four buttons when you log-in as first responder and two buttons for Civilian.
  
When you are a first responder  

1. "There is an EarthQuake!"  
This button is not activated as default however it will enabled either the app detects the shakes (EarthQuake!) or user manually clicked the button. When this button is on, the text is changed it to "Joined Emergency Network". It means that you have joined into a group of network in emergency channel.

2. "Search Nearby Users"  
The phone search the nearby users(the phone with app installed) automatically by using one of the following methods: Bluetooth, Wi-Fi (infrastructure network) or peer-to-peer connection. Once the phone found the users, it will display on the list.  
-> When you click one of the list of user, you will be able to chat with that user like a messenger and also to be able to send message and trigger the alarm.

3. "Send Broadcast Messages"   
You are able to send Broadcast Message to everyone who joined in emergency channel.

4. "Civilian Record"  
It will display the list of Civilian's information such as location, address and how much far away from your current location.  
-> When you click one of the list, it will track the indicated user's location and display on google map.

If you are a Civilian  
  
1. "There is an EarthQuake!"  
Same functionality from first responder. But unlike first responder, Civilian only can recieve broadcast message.  

2. "Search Nearby Users"    
Same functionality from first responder.

**Shake detection**  
The system will monitor the sensors within a user's smartphone to try to detect potential earthquake activity.  
Once an activity (shake) is detected, the application will check the user's condition whether they are injured. It is then sent to an API for decision-making, if the API has received substantial amount of data (For now, only if there are more than three events within the certain amount of time), the data will be sent to the server, it will decide whether it is considered an earthquake.  
The saved user's data displayed in a list and can be seen by clicking the "Civilian Record" button from the main menu.

# About this project
---
This project is aimed at developing an application to restore communication in affected disaster areas. The project can work with Bluetooth, Wi-Fi direct or regular infrastructure Wi-Fi. Various features in this app facilitate communication between civilians (Victims) and first responders which include; sending broadcast notifications, sending 1 to 1 encrypted messages, asking about user's injury state, storing user location and other information locally in a database or on the cloud, providing a list of affected civilians to the first responder within any area, display that list of civilians on a map to help first responder respond quicker, enable chat to send message, images, or send alerts. also track the user by using augmented reality view and wearable devices to get the civilian location with guided directions. The focus on developing a local mesh network and wearable computing will aid in accomplishing the application's main goals. The project is versatile enough to adapt to other disasters in the future such as tsunami or even blackouts.

 

# Solution Impact
---
This solution will help emergency responders quickly determine within a small margin of error the number of trapped people inside the building and assist them in devising a rescue plan according to the information they received. 

First responders will benefit from the mesh network and chatting system to effectively reduce the amount of time searching for civilians. The civilians get benefit from the chatting system to increase the chance of survivability to contact with nearby civilians or first responders. One core function is to locate other users of the application using AR who are stuck in inaccessible areas. This should be feasible within the Capstone timeframe and more features can be added to improve the application's usefulness. 

# Roles (Civilian vs First Responders)
---
1. Civilian - Any civilians who needs help (even when they are trapped inside a building) can use the app to connect their devices to others in order to manage their communication to the outside world. 
	* Discover nearby user. 
	* Chat with either first responder or other Civilians to ask for help. 
	* Can send a picture to nearby users.  
	* Can see the other user's location by using Augmented Reality.
	
2. First Responder - First responder is a person who is rescuing Civilian.  
	* Can do everything mentioned above 
	* Send broadcast messages 
	* Can see the list of user's data where the shake events happened. First responder can review the address, check injury state and how far they are away from the first responder.  
	* Can see user's location based on records on the map. 
	* Trigger an alarm in Civilian's phone. 
	
3. First Responder (Wearing a smartwatch) - First Responder with access to a smartwatch 
	* Discover nearby users 
	* Send predefined messages 
	* Arrow pointer towards selected Civilian 
	* Trigger an alarm in Civilian's phone.
	
# Authors
---
## Supervisor  
* Professor Kevin Forest   

## Developers 
* Ahmed Elmoselhy 
* Joo Hyun Park 
* Kenji Casibang

## Publisher
* Sheridan College


# Resources 
---
This project has been implemented "Hype SDK" from HypeLabs for embedded advanced Mesh Networking technology for security and interoperability purposes.

**Why Hype SDK?**  
Using the Hype SDK, we could create our own local mesh network using most of the available transports on a smartphone. Depending on the transports available a user with only Bluetooth enabled and a user with only Wi-Fi-direct enabled will be able to communicate if there is another user with both. Each user will be able to communicate with others in a fast and efficient manner compared to traditional Bluetooth and Wi-Fi Direct methods. Broadcasting messages will also be possible to relay information to all connected users. The SDK supports user authentication, end to end encryption and network segregation. It is also available on multiple platforms such as Windows, iOS, and Linux which can be used for future work. 

Please refer this link to see more detail about Hypelab.  
[https://hypelabs.io/](https://hypelabs.io/)

# License
---
[MIT License Link](https://choosealicense.com/licenses/mit/).


